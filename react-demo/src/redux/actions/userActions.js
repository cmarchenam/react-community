export const USERS_LOAD = 'USERS_LOAD';
const internalLoad = (users) => {
  return {
    type: USERS_LOAD,
    users
  }
}

export const loadUsers = users => {
    return (dispatch) => {
        if (!users) {
            fetch('https://jsonplaceholder.typicode.com/users')
                .then(userResponse => userResponse.json())
                .then(json => dispatch(internalLoad(json)))
            // dispatch(internalLoad({id: 1, name: 'alex', email:'lol', website:'aham'}))
        }
        else {
            dispatch(internalLoad(users))
        }
    }
};

export const USERS_FILTER = 'USERS_FILTER';
export const filterUsers = (term) => {
    return {
        type: USERS_FILTER,
        term
    }
}