import { USERS_LOAD, USERS_FILTER } from '../actions/userActions';

const UserReducers = (state = { term: '', users: [] }, action) => {
    switch (action.type) {
      case USERS_LOAD:
        return { ...state,
          users: action.users.map((u) => {
              return Object.assign(u);
            })};
      case USERS_FILTER:
        return { ...state,
          term: action.term.toLowerCase()
        }
      default:
        return state;
    }
};
  
export default UserReducers;