import React from 'react';

function About() {
  return (
    <main className="container-fluid">
        <section>
          <h1>About React</h1>
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Component-Based</h4>
              <p className="card-text">
                Build encapsulated components that manage their own state, then compose them to make complex UIs.
                Since component logic is written in JavaScript instead of templates,
                you can easily pass rich data through your app and keep state out of the DOM.
              </p>
              <p>
                Don't miss to review...
              </p>
              <a type="button" className="btn btn-outline-info ml-3"
                href="https://reactjs.org/" target="_blank" rel="noopener noreferrer">
                  reactjs.org
              </a>

              <a type="button" className="btn btn-outline-info ml-3"
                href="https://reactjs.org/docs/hooks-overview.html" target="_blank" rel="noopener noreferrer">
                  hooks
              </a>

              <a type="button" className="btn btn-outline-info ml-3"
                href="https://reactrouter.com/web/guides/quick-start"
                target="_blank"
                rel="noopener noreferrer">
                  react-router-dom
              </a>

              <a type="button" className="btn btn-outline-info ml-3"
                href="https://react-hook-form.com/"
                target="_blank"
                rel="noopener noreferrer">
                  react-hook-form
              </a>

              <a type="button" className="btn btn-outline-info ml-3"
                href="https://react-redux.js.org/"
                target="_blank"
                rel="noopener noreferrer">
                  react-redux
              </a>
            </div>
          </div>
        </section>
    </main>
  );
}

export default About;
