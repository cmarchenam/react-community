import React from 'react';
import './Nav.css';
import { Link } from 'react-router-dom';

function Nav() {
  return (
    <nav>
        <ul className="nav-links">
            <Link to="/" >
                <li className="link">Home</li>
            </Link>
            <Link to="/user">
                <li className="link">Users</li>
            </Link>
            <Link to="/userform">
                <li className="link">User Form</li>
            </Link>
            <Link to="/external" >
                <li className="link">Third Party API</li>
            </Link>
            <Link to="/about" >
                <li className="link">About</li>
            </Link>
        </ul>
    </nav>
  );
}

export default Nav;
