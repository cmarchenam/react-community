import React from 'react';
import logo from './logo.svg'
import './Home.css';

const Home = () => (
  <main className="home">
      <img src ={logo} alt="logo" width="500" height="500" />
  </main>
);

export default Home;
