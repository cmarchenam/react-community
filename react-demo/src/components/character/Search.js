import React, {useState} from 'react';
import './External.css';

const Search = ({getQuery}) => {
    const [text, setText] = useState('');
    const onChange = (q) => {
        setText(q);
        getQuery(q);
    };

    return (
        <section className="search-container">
            <form>
                <input 
                    type="text" 
                    placeholder="Search Characters" 
                    value={text} 
                    onChange={(e) => onChange(e.target.value)}
                    autoFocus />
            </form>
        </section>
    );
};

export default Search;
