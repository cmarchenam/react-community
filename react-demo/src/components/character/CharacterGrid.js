import React from 'react';
import './External.css';

const CharacterGrid = ({isLoading, items}) => {
  return isLoading ? (<h1>Loading...</h1>) : (
    <section className="cards">
        {items.map(
            item => (
              <div className='card' key={item.char_id}>
                <div className='card-inner'>
                  <div className='card-front'>
                    <img src={item.img} alt='' />
                  </div>
                  <div className='card-back'>
                    <h1>{item.name}</h1>
                    <ul>
                      <li>
                        <strong>Actor Name:</strong> {item.portrayed}
                      </li>
                      <li>
                        <strong>Nickname:</strong> {item.nickname}
                      </li>
                      <li>
                        <strong>Birthday:</strong> {item.birthday}
                      </li>
                      <li>
                        <strong>Status:</strong> {item.status}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            )
        )}
    </section>)
}

export default CharacterGrid;