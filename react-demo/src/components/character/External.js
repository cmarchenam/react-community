import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './External.css';
import CharacterGrid from './CharacterGrid';
import Search from './Search';

const External = () => {
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [query, setQuery] = useState('');

  useEffect(() => {
    const fetchItems = async() => {
      const result = await axios(`https://www.breakingbadapi.com/api/characters?name=${query}`);

      setItems(result.data);
      setIsLoading(false);
    }

    fetchItems();
  }, [query])

  return (
    <div>
        <Search getQuery={(q) => setQuery(q)}/>
        <CharacterGrid isLoading={isLoading} items={items} />
    </div>
  );
}

export default External;