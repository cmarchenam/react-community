import React from 'react';
import './App.css';
import Header from '../header/Header';
import Nav from '../nav/Nav';
import Home from '../home/Home';
import UserList from '../user/UserList';
import UserPost from '../user/UserPost';
import UserForm from '../user/User';
import About from '../about/About';
import External from '../character/External';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <div>
      <Header />
      <Router>
          <Nav />
          <Switch>
            <Route path="/" exact>
              <Home/>
            </Route>
            <Route path="/user" exact >
              <UserList/>
            </Route>
            <Route path="/user/:id">
              <UserPost/>
            </Route>
            <Route path="/userform">
              <UserForm/>
            </Route>
            <Route path="/about">
              <About/>
            </Route>
            <Route path="/external">
              <External/>
            </Route>
          </Switch>
      </Router>
    </div>
  );
}

export default App;
