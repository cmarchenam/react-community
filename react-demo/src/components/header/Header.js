import React from 'react';
import './Header.css';

function Header() {
  return (
    <header>
        <h1 style={{display: "flex", justifyContent: "center", marginTop: "5px"}}>
            Learning React
        </h1>
    </header>
  );
}

export default Header;