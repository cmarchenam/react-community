import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { loadUsers, filterUsers } from '../../redux/actions/userActions';

import UserListRow from './UserListRow';

const UserList = (props) => {
  const { users, loadUsers : loads , term} = props;
  // const [searchTerm, setSearchTerm] = useState(term);
  if (users.length === 0) {
    loads();
  }
  const [userList, setUsers] = useState([]);

  useEffect(() => {
    if (users.length === 0) {
      loads();
    }
    setUsers(users);

    return () => setUsers([]);
  }, [users, loads]);

  const performSearch = event => {
    const { filterUsers: filterSearch } = props;
    filterSearch(event.target.value);
  }

  return (
    <main className="container-fluid" >
      <section>
        <h1>User List</h1>
        <form autoComplete="off" onSubmit={(e) => e.preventDefault()}>
          <label htmlFor="searchUser">Buscar Usuario:&nbsp;</label>
          <input 
            type="text" 
            id="searchUser" 
            onChange={performSearch}
          />
        </form>
        <table className="table">
          <thead className="bg-light">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Website</th>
              <th scope="col">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            {userList.map((user) => {
              return (
                // <tr key={user.id}>
                //   <td>{user.name}</td>
                //   <td>{user.email}</td>
                //   <td>{user.website}</td>
                //   <td>
                //     <span style={{marginLeft: "8px"}}>
                //         <Link to={`/user/${user.id}`}>Posts</Link>
                //     </span>
                //   </td>
                // </tr>
                <UserListRow user={user} key={user.id} />
              )
            })
            }
          </tbody>
        </table>
      </section>
    </main>
  );
}

const filter = (u, term) => {
  return u.name.toLowerCase().indexOf(term) > -1 || u.email.toLowerCase().indexOf(term) > -1 || u.website.toLowerCase().indexOf(term) > -1;
}

const mapStateToProps = state => ({
  users: state.UserReducers.users.filter((u) => { return filter(u, state.UserReducers.term)}),
  term: state.UserReducers.term || '',
})

export default connect(
  mapStateToProps, { loadUsers, filterUsers }  
)(UserList);
