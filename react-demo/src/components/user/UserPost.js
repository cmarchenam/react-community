import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';

function UserPost() {
  const params = useParams();

  const history = useHistory();

  const [userPosts, setUserPosts] = useState([]);

  useEffect(() => {
    getUserPosts(params.id);
    return () => setUserPosts([]);
  }, [params.id]);

  const getUserPosts = async (id) => {
    let url = `https://jsonplaceholder.typicode.com/posts?userId=${id}`;

    const response = await fetch(url);
    const data = await response.json();
    setUserPosts(data);
    console.log(data);
  };

  const goBack = () => history.goBack();

  return (
    <main className="container-fluid">
      <section>
         <h1>User Posts</h1>
         <button class="btn btn-outline-info mb-2" onClick={goBack}>Go back</button>
         <div className="card-columns">
         {
           userPosts.map((post) => {
            return (
              <div key={post.id} className="card border border-info bg-light" style={{width: "26rem"}}>
                <div className="card-body">
                  <h3 className="card-title">{post.title}</h3>
                  <p className="card-text">{post.body}</p>
                </div>
              </div>
              );
           })
         }
         </div>
      </section>
    </main>
  );
}

export default UserPost;
