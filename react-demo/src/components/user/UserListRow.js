import React from 'react';
import { Link } from 'react-router-dom';

function UserListRow({user}) {
    console.log(user)
    return (
        <tr key={user.id}>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td>{user.website}</td>
            <td>
            <span style={{marginLeft: "8px"}}>
                <Link to={`/user/${user.id}`}>Posts</Link>
            </span>
            </td>
      </tr>)
}

export default UserListRow;