import React from 'react';
import { useForm } from 'react-hook-form';

function User() {

  const { register, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
      console.log(data);
      console.log(errors);
  }

  return (
    <main className="container-fluid">
      <section className="row mt-1">
        <div className="col-md-6">
          <h1>User</h1>
          <div>
            <form onSubmit = { handleSubmit(onSubmit) }>
              <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <input type="text"
                  name="firstName"
                  placeholder="First name"
                  className="form-control form-control-sm"
                  ref={register({ required: true, maxLength: 50 })} />
                <small style={{color: "red"}}>
                  {errors.firstName && "First name is required"}
                </small>
              </div>

              <div className="form-group">
                <label htmlFor="LastName">Last Name</label>
                <input type="text"
                  placeholder="Last name"
                  name="lastName"
                  className="form-control form-control-sm"
                  ref={register({ required: true, maxLength: 50 })} />
                <small style={{color: "red"}}>
                  {errors.lastName && "Last name is required"}
                </small>
              </div>

              <div className="form-group">
                <label htmlFor="Email">Email</label>
                <input type="text"
                  placeholder="Email"
                  name="email"
                  className="form-control form-control-sm"
                  ref={register({ required: true, maxLength: 50, pattern: /^\S+@\S+$/i })} />
                <small style={{color: "red"}}>
                  {errors.lastName && "Email is required"}
                </small>
              </div>

              <div className="form-group">
                <input type="submit" className="btn btn-outline-info" value="Create User"/>
              </div>
            </form>
          </div>
        </div>
      </section>
    </main>
  );
}

export default User;
